import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String kroaeGreeting = "헬로 플러터";
String japanGreeting = "こんにちはフラッター";


class _MyStatefulWidgetState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;
     @override
   Widget build(BuildContext context){
     return MaterialApp(
       debugShowCheckedModeBanner: false,
       //Scaffold Widget
       home: Scaffold(
         appBar: AppBar(
           title: Text("Hello Flutter"),
           leading: Icon(Icons.home),
           actions: <Widget>[
             IconButton(
                 onPressed: () {
                   setState(() {
                     displayText = displayText == englishGreeting?
                     spanishGreeting : englishGreeting;
                   });
                 } ,
                 icon: Icon(Icons.refresh)),
             IconButton(
                 onPressed: () {
                   setState(() {
                     displayText = displayText == englishGreeting?
                     kroaeGreeting : englishGreeting;
                   });
                 } ,
                 icon: Icon(Icons.heart_broken)),
             IconButton(
                 onPressed: () {
                   setState(() {
                     displayText = displayText == englishGreeting?
                     japanGreeting : englishGreeting;
                   });
                 } ,
                 icon: Icon(Icons.favorite))
           ],
         ),
         body: Center(
          child: Text(
             displayText,
            style: TextStyle(fontSize: 24),
          ),
         ),
      ),
    );
}
}





// class HelloFlutterApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context){
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       //Scaffold Widget
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(
//                 onPressed: () {} ,
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//             "Hello Flutter",
//             style: TextStyle(fontSize: 24),
//           ),
//         ),
//       ),
//     );
//   }
// }